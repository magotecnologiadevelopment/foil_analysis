package co.edu.uan.foilanalysis.fragments

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import co.edu.uan.foilanalysis.R

class MeasurePreferenceFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.pref_info, rootKey)
    }
}
