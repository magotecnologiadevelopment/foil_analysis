package co.edu.uan.foilanalysis.usecases

import org.opencv.core.Core
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc

class ImageBeautifierImpl(val brightness: Double = 0.0, val contrast: Double = 1.0, val sharpness: Double = 0.0) :
    ImageBeautifier {
    override fun applyFilters(imageRGB: Mat): Mat {
        // resultingImage=applySharpness(imageRGB = resultingImage,value = sharpness)
        return applyEquation(imageRGB, contrast, brightness)
    }

    override fun applyBrightness(imageRGB: Mat, value: Double): Pair<Mat, Long> {
        val startProcessTime = System.currentTimeMillis()
        val returnImage = applyEquation(imageRGB, 1.0, value)
        val endProcessTime = System.currentTimeMillis()
        val processTime = endProcessTime - startProcessTime
        return Pair(returnImage, processTime)
    }

    override fun applySharpness(imageRGB: Mat, value: Double): Mat {
        TODO("Se debe crear esta funcion")
    }

    override fun reduceGaussianBlur(grayRGB: Mat): Mat {
        val blurred = Mat()
        Imgproc.GaussianBlur(grayRGB, blurred, Size(1.0, 1.0), 0.0)
        Core.addWeighted(grayRGB, 1.5, blurred, -0.5, 0.0, grayRGB)
        return grayRGB
    }

    private fun applyEquation(imageRGB: Mat, contrast: Double, brightness: Double): Mat {
        val resultingImage = Mat()
        Core.addWeighted(imageRGB, contrast, imageRGB, 0.0, brightness, resultingImage)
        return resultingImage
    }
}
