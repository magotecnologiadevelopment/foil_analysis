package co.edu.uan.foilanalysis.extentions

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageFormat
import android.media.Image
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.Script
import android.renderscript.Type
import org.opencv.android.Utils
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import java.nio.ByteBuffer

typealias alloc = Allocation

@Suppress("ComplexMethod", "NestedBlockDepth")
fun Image.toMat(): Mat {
    var buffer: ByteBuffer
    var rowStride: Int
    var pixelStride: Int
    val width = this.width
    val height = this.height
    var offset = 0
    val bitsByPixel = 8
    val planes = this.planes
    val data = ByteArray(this.width * this.height * ImageFormat.getBitsPerPixel(ImageFormat.YUV_420_888) / bitsByPixel)
    val rowData = ByteArray(planes[0].rowStride)
    for (i in 0 until planes.size) {
        buffer = planes[i].buffer
        rowStride = planes[i].rowStride
        pixelStride = planes[i].pixelStride
        val w: Int = if (i == 0) width else width / 2
        val h: Int = if (i == 0) height else height / 2
        for (row in 0 until h) {
            val bytesPerPixel: Int = ImageFormat.getBitsPerPixel(ImageFormat.YUV_420_888) / bitsByPixel
            if (pixelStride == bytesPerPixel) {
                val length = w * bytesPerPixel
                buffer.get(data, offset, length)
                if (h - row != 1) {
                    buffer.position(buffer.position() + rowStride - length)
                }
                offset += length
            } else {
                if (h - row == 1) {
                    buffer.get(rowData, 0, width - pixelStride + 1)
                } else {
                    buffer.get(rowData, 0, rowStride)
                }
                for (col in 0 until w) {
                    data[offset++] = rowData[col * pixelStride]
                }
            }
        }
    }
    val mat = Mat(this.height + this.height / 2, this.width, CvType.CV_8UC1)
    mat.put(0, 0, data)
    return mat
}

fun Image.yUVtoRGB(context: Context): Bitmap? {
    val planes = this.planes
    val buffer: ByteBuffer = planes[0].buffer
    val yuvData = bufferToYuvPlanes(buffer, planes)
    val yRowStride = planes[0].rowStride
    val uvRowStride = planes[1].rowStride // we know from   documentation that RowStride is the same for u and v.
    val uvPixelStride = planes[1].pixelStride
    val rs: RenderScript = RenderScript.create(context)
    val yuv420 = configureScriptYuv(rs, yuvData, width, height, yRowStride)
    yuv420._uvRowStride = uvRowStride.toLong()
    yuv420._uvPixelStride = uvPixelStride.toLong()
    return renderScriptToBitmap(rs = rs, width = width, height = height, script = yuv420)
}

fun Image.jpegToBitmap(): Bitmap? {
    val buffer = this.planes[0].buffer
    buffer.rewind()
    val bytes = ByteArray(buffer.remaining())
    buffer.get(bytes)
    var bufMat = Mat(Size(this.width.toDouble(), this.height.toDouble()), CvType.CV_8UC1)
    bufMat.put(0, 0, bytes)
    var convertedMat = Mat(Size(this.width.toDouble(), this.height.toDouble()), CvType.CV_8UC3)
    bufMat = Imgcodecs.imdecode(bufMat, Imgcodecs.CV_LOAD_IMAGE_UNCHANGED)
    val colorMat = Mat()
    Imgproc.cvtColor(bufMat, colorMat, Imgproc.COLOR_BGR2RGB)
    val bitmap = Bitmap.createBitmap(colorMat.cols(), colorMat.rows(), Bitmap.Config.ARGB_8888)
    Utils.matToBitmap(colorMat, bitmap)
    return bitmap
}

fun Image.jpgToBitmap(): Bitmap {
    val buffer = this.planes[0].buffer
    buffer.rewind()
    val bytes = ByteArray(buffer.remaining())
    buffer.get(bytes)
    val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
    val matTest = Mat()
    Utils.bitmapToMat(bitmap, matTest)
    return bitmap
}

fun bufferToYuvPlanes(incomingBuffer: ByteBuffer, planes: Array<Image.Plane>): Triple<ByteArray, ByteArray, ByteArray> {
    var buffer = incomingBuffer
    buffer.rewind()
    val y = ByteArray(buffer.remaining())
    buffer.get(y)
    buffer = planes[1].buffer
    val u = ByteArray(buffer.remaining())
    buffer.get(u)
    buffer = planes[2].buffer
    val v = ByteArray(buffer.remaining())
    buffer.get(v)
    return Triple(y, u, v)
}

fun renderScriptToBitmap(rs: RenderScript, width: Int, height: Int, script: ScriptC_yuv420888): Bitmap {
    val outBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val outAlloc = alloc.createFromBitmap(rs, outBitmap, Allocation.MipmapControl.MIPMAP_NONE, alloc.USAGE_SCRIPT)
    val launchOptions = Script.LaunchOptions()
    launchOptions.setX(0, width)
    launchOptions.setY(0, height)
    script.forEach_doConvert(outAlloc, launchOptions)
    outAlloc.copyTo(outBitmap)
    return outBitmap
}

fun configureScriptYuv(
    rs: RenderScript,
    yuvData: Triple<ByteArray, ByteArray, ByteArray>,
    width: Int,
    height: Int,
    yRowStride: Int
): ScriptC_yuv420888 {
    val yuv420 = ScriptC_yuv420888(rs)
    val typeUcharY = Type.Builder(rs, Element.U8(rs))
    typeUcharY.setX(yRowStride).setY(height)
    val yAlloc = alloc.createTyped(rs, typeUcharY.create())
    yAlloc.copy1DRangeFrom(0, yuvData.first.size, yuvData.first)
    yuv420._ypsIn = yAlloc
    val typeUcharUV = Type.Builder(rs, Element.U8(rs))
    // note that the size of the u's and v's are as follows:
    //      (  (width/2)*PixelStride + padding  ) * (height/2)
    // =    (RowStride                          ) * (height/2)
    // but I noted that on the S7 it is 1 less...
    typeUcharUV.setX(yuvData.second.size)
    val uAlloc = Allocation.createTyped(rs, typeUcharUV.create())
    uAlloc.copyFrom(yuvData.second)
    yuv420._uIn = uAlloc
    val vAlloc = Allocation.createTyped(rs, typeUcharUV.create())
    vAlloc.copyFrom(yuvData.third)
    yuv420._vIn = vAlloc
    // handover parameters
    yuv420._picWidth = width.toLong()
    return yuv420
}
