package co.edu.uan.foilanalysis.usecases.exceptions

class UnknownThresholdTypeException : Exception()
