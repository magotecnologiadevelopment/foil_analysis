package co.edu.uan.foilanalysis.usecases

import co.edu.uan.foilanalysis.model.Hole
import org.opencv.core.Mat

interface Analyzer {
    fun findCircles(grayImage: Mat): Pair<Mat,List<Hole>>
}
