package co.edu.uan.foilanalysis.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.ImageFormat
import android.graphics.Point
import android.graphics.SurfaceTexture
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.media.ImageReader
import android.os.Bundle
import android.util.Size
import android.view.Surface
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import co.edu.uan.foilanalysis.R
import co.edu.uan.foilanalysis.activities.utils.CompareSizeByArea
import co.edu.uan.foilanalysis.activities.utils.PermissionManager
import co.edu.uan.foilanalysis.extentions.setListeners
import co.edu.uan.foilanalysis.viewmodels.CalibratorViewModel
import kotlinx.android.synthetic.main.activity_capture.*
import org.jetbrains.anko.debug
import org.jetbrains.anko.error
import org.jetbrains.anko.info
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.verbose
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

class CalibratorActivity : Camera2Activity() {
    private lateinit var vm: CalibratorViewModel
    /**
     * Esta clase gestiona la verificación y solicitud de los permisos
     */
    private val permissionManager = PermissionManager(this)
    // Tamaños de pantalla
    private lateinit var previewSize: Size
    private lateinit var preferedSize: Size
    private lateinit var sharedPreferences: SharedPreferences
    private var needResume: AtomicBoolean = AtomicBoolean(true)
    private var fingerSpacing: Float = CalibratorActivity.zoomDefaultValues
    private var zoomLevel: Float = CalibratorActivity.zoomDefaultValues
    private var openCVisOpen = false
    private val mOpenCVCallBack = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    log.info("OpenCV loaded successfully")
                    openCVisOpen = true
                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calibrator)
        // Crea y se enlaza con un ViewModel
        vm = ViewModelProviders.of(this)[CalibratorViewModel::class.java]
        // Se observa la propiedad image del VM y si se cambia realiza el metodo updateImageView
        vm.imageToShow.observe(this, Observer { updateImageView(it) })
        setButtonListeners()
    }

    private fun updateImageView(it: Bitmap?) {
    }

    private fun setButtonListeners() {
        // El boton de captura inicia el proceso al bloquear el foco
        btn_capture.setOnClickListener {
            previewTextu.visibility = View.VISIBLE
            image_preview.visibility = View.VISIBLE
            lockFocus()
        }
        // Abre el activity de Configuración
        btn_settings.setOnClickListener {
            startActivity(intentFor<SettingsActivity>())
        }
        log.verbose("Se asignaron los comportamientos a los botones")
    }

    @SuppressLint("MissingPermission")
    public override fun onResume() {
        super.onResume()
        if (needResume.get()) {
            if (!OpenCVLoader.initDebug()) {
                OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mOpenCVCallBack)
            } else {
                System.loadLibrary("opencv_java3")
                log.info("OpenCV library found inside package, use it!")
                mOpenCVCallBack.onManagerConnected(LoaderCallbackInterface.SUCCESS)
            }
            // Se obtienen las preferencias propias
            // getPreferences()
            startBackground()
            // Revisa los permisos
            val allPermissionsGranted = permissionManager.checkAllPermissions()
            // Inicia el trabajo de actualización de la imagen
            if (allPermissionsGranted) {
                if (!cameraOpenCloseLock.tryAcquire(CalibratorActivity.timeOutTime, TimeUnit.MILLISECONDS)) {
                    log.error("Time out waiting to lock camera opening.")
                }
                // Se abre la camara
                if (openCVisOpen) {
                    if (previewTextu.isAvailable) {
                        surfaceTexture = Surface(previewTextu?.surfaceTexture)
                        openCamera(preferedSize)
                    } else {
                        previewTextu.setListeners(avalaibleFunction = { _, width, height ->
                            surfaceTexture = Surface(previewTextu?.surfaceTexture)
                            openCamera(preferedSize)
                        })
                    }
                    log.debug(message = "Se han concedido todos los permisos")
                }
            }
        }
    }

    override fun setUpCameraOutputs(outputSize: Size) {
        val manager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            val characteristics = manager.getCameraCharacteristics(cameraId)
            // Obtiene los tamaños soportados por la camara
            val map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
            // TODO: Revisar mejor opcion
            val largest = (map.getOutputSizes(SurfaceTexture::class.java)).maxWith(CompareSizeByArea())
            val swappedDimmensions = false
            val displaySize = Point()
            windowManager.defaultDisplay.getSize(displaySize)
            // Si las dimensiones son cambiadas, cambia los tamaños de visualización
            val rotatedPreviewWidth = outputSize.width
            val rotatedPreviewHeight = outputSize.height
            val maxPreviewWidth = displaySize.x
            val maxPreviewHeight = displaySize.y
            zoom = characteristics.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE)
            // Obtiene el mejor tamaño para visualizar
            previewSize = CompareSizeByArea.chooseOptimalSize(
                map.getOutputSizes(ImageFormat.JPEG), Size(rotatedPreviewWidth, rotatedPreviewHeight),
                Size(maxPreviewWidth, maxPreviewHeight), largest
            )
            previewSize = preferedSize
            // Crea un receptor del tamaño seleccionado  y formato YUV para tratar la imagen
            previewReader = ImageReader.newInstance(
                previewSize.width,
                previewSize.height,
                ImageFormat.YUV_420_888,
                CalibratorActivity.maxReaderCapacity
            )
            // Le asigna respuesta al receptor
            previewReader?.setOnImageAvailableListener(
                { reader ->
                    val image = reader.acquireNextImage()
                    processPreview(image)
                    image.close()
                },
                backgroundHandler
            )
            // Crea un receptor del tamaño seleccionado  y formato YUV para cuando se tome  la imagen
            captureReader = ImageReader.newInstance(
                preferedSize.width,
                preferedSize.height,
                ImageFormat.JPEG,
                CalibratorActivity.maxReaderCapacity
            )
            captureReader?.setOnImageAvailableListener(
                { reader ->
                    val image = reader.acquireNextImage()
                    // loadImage()
                    image.close()
                },
                backgroundHandler
            )
            surfaces = Arrays.asList(captureReader?.surface, previewReader?.surface, surfaceTexture)
        } catch (e: CameraAccessException) {
            log.error(e.printStackTrace())
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            onResume()
        }
    }

    companion object {
        private const val zoomStep = 3
        private const val percentDiv = 100
        private const val timeOutTime: Long = 2500
        private const val maxReaderCapacity = 3
        private const val zoomDefaultValues = 0.0F
    }
}
