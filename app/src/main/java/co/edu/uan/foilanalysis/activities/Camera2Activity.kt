package co.edu.uan.foilanalysis.activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.ImageFormat
import android.graphics.Rect
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraCaptureSession
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.CameraManager
import android.hardware.camera2.CameraMetadata
import android.hardware.camera2.CaptureRequest
import android.hardware.camera2.CaptureResult
import android.hardware.camera2.TotalCaptureResult
import android.media.Image
import android.media.ImageReader
import android.os.Handler
import android.os.HandlerThread
import android.provider.ContactsContract
import android.util.Log
import android.util.Size
import android.view.Surface
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import co.edu.uan.foilanalysis.activities.exceptions.InterruptedCameraClosingException
import co.edu.uan.foilanalysis.extentions.createSession
import co.edu.uan.foilanalysis.extentions.getBasicStateResponse
import co.edu.uan.foilanalysis.managers.CaptureState
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import java.util.concurrent.Semaphore

@SuppressLint("Registered")
open class Camera2Activity : AppCompatActivity() {
    var cameraDevice: CameraDevice? = null
    lateinit var cameraId: String
    // ImageReader que son receptores de la visualización
    protected var previewReader: ImageReader? = null
    protected var captureReader: ImageReader? = null
    // Manejo de las hilos
    private var backgroundThread: HandlerThread? = null
    protected var backgroundHandler: Handler? = null
    protected val log = AnkoLogger(this.javaClass)

    /**
     * Un [Semaphore] que evita que la aplicación se cierre antes de cerrar la cámara.
     */
    var cameraOpenCloseLock = Semaphore(1)
    // Estado de la captura de imagen
    var state = CaptureState.STATE_PREVIEW
    // Cuadro que contiene la imagen para el Zoom
    lateinit var zoom: Rect
    // Sesiones de captura
    var cameraRequestBuilder: CaptureRequest.Builder? = null
    protected var previewSession: CameraCaptureSession? = null
    // Lista de las superficies que se añadiran a la sesion de la camara
    protected var surfaces = mutableListOf<Surface>()
    // Surface del textureView
    // TODO: Remover surfaceTexture
    lateinit var surfaceTexture: Surface
    // Foco de visualización
    private var focus: String = "5.0"
    // Callback usado para mantener un flujo en la captura de la imagen
    private val captureCallback = object : CameraCaptureSession.CaptureCallback() {
        // Ambos sucesos se resuelven a través del método process
        override fun onCaptureProgressed(
            session: CameraCaptureSession,
            request: CaptureRequest,
            partialResult: CaptureResult
        ) {
            process(partialResult)
        }

        override fun onCaptureCompleted(
            session: CameraCaptureSession,
            request: CaptureRequest,
            result: TotalCaptureResult
        ) {
            process(result)
        }

        private fun process(result: CaptureResult) {
            when (state) {
                CaptureState.STATE_PREVIEW -> Unit // Si está en previsualización no hacer nada
                CaptureState.STATE_WAITING_LOCK -> capturePicture(result)
                CaptureState.STATE_WAITING_PRECAPTURE -> {
                    // En algunos dispositivos CONTROL_AE_STATE puede resultar nulo
                    val aeState = result.get(CaptureResult.CONTROL_AE_STATE)
                    if (aeState == null ||
                        aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE ||
                        aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED
                    ) {
                        state = CaptureState.STATE_WAITING_NON_PRECAPTURE
                    }
                }
                CaptureState.STATE_WAITING_NON_PRECAPTURE -> {
                    // En algunos dispositivos CONTROL_AE_STATE puede resultar nulo
                    val aeState = result.get(CaptureResult.CONTROL_AE_STATE)
                    if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
                        state = CaptureState.STATE_PICTURE_TAKEN
                        capture()
                    }
                }
                CaptureState.STATE_PICTURE_TAKEN -> Unit
            }
        }
    }

    /**
     * Método que realiza la configuración de apertura de la cámara
     */
    protected fun openCamera(outputSize: Size) {
        // Se configuran los valores de las salidas
        setUpCameraOutputs(outputSize)
        // Se revisa dinamicamente el permiso de la camara
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            val manager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
            manager.openCamera(
                cameraId,
                getBasicStateResponse(this, openedFunction = { startPreview() }), backgroundHandler
            )
        }
    }

    /**
     * Método que establece los receptores de la imagenes de captura y previsualización
     */
    protected open fun setUpCameraOutputs(outputSize: Size) {
        // Crea un receptor del tamaño seleccionado  y formato YUV para tratar la imagen
        previewReader = ImageReader.newInstance(
            outputSize.width,
            outputSize.height,
            ImageFormat.YUV_420_888, maxReaderCapacity
        )
        // Le asigna respuesta al receptor
        previewReader?.setOnImageAvailableListener(
            { reader ->
                val image = reader.acquireNextImage()
                processPreview(image)
                image.close()
            },
            backgroundHandler
        )
        // Crea un receptor del tamaño seleccionado  y formato JPEG para cuando se tome  la imagen
        captureReader = ImageReader.newInstance(
            outputSize.width,
            outputSize.height,
            ImageFormat.JPEG,
            maxReaderCapacity
        )
        captureReader?.setOnImageAvailableListener(
            { reader ->
                val image = reader.acquireNextImage()
                processCapture(image)
                image.close()
            },
            backgroundHandler
        )
    }

    /**
     * Método que será sobrescrito con el procesamiento de la imagen capturada
     * @param image: Imagen a procesar
     */
    protected open fun processCapture(image: Image?) {
    }

    /**
     * Método que será sobrescrito con el precesamiento de la previsualización
     * @param image: Imagen a procesar
     */
    protected open fun processPreview(image: Image?) {
    }

    /**
     *Método para el inicio de la previsualización
     */
    private fun startPreview() {
        try {
            cameraRequestBuilder = cameraDevice?.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
            // Establece que el balance de blancos sea automatico, auto foco se adapte al movimiento y otros controles
            cameraRequestBuilder?.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO)
            cameraRequestBuilder?.set(CaptureRequest.CONTROL_AWB_MODE, CameraMetadata.CONTROL_AWB_MODE_AUTO)
            cameraRequestBuilder?.set(CaptureRequest.CONTROL_AE_MODE, CameraMetadata.CONTROL_AE_MODE_ON)
            cameraRequestBuilder?.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)
            cameraRequestBuilder?.set(
                CaptureRequest.NOISE_REDUCTION_MODE,
                CaptureRequest.NOISE_REDUCTION_MODE_HIGH_QUALITY
            )
            cameraRequestBuilder?.set(CaptureRequest.LENS_FOCUS_DISTANCE, java.lang.Float.parseFloat(focus))
            val surfaceFromPreviewReader = previewReader?.surface
            if (surfaceFromPreviewReader != null) {
                cameraRequestBuilder?.addTarget(surfaceFromPreviewReader)
            }
            cameraRequestBuilder?.addTarget(surfaceTexture)
            // Se crea una sesión, con las superficies inscritas y su controlador
            cameraDevice?.createSession(onConfigFunction = { captureSession ->
                previewSession = captureSession
                updatePreview(cameraRequestBuilder)
            }, surfaces = surfaces, handler = backgroundHandler)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    /**
    Realiza la configuración para actualizar la previsualización
     */
    private fun updatePreview(previewBuilder: CaptureRequest.Builder?) {
        try {
            // Crea un nuevo hilo
            previewBuilder?.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO)
            val thread = HandlerThread("preview")
            thread.start()
            // Establece la sesión de previsualización como repetitiva (continua)
            previewSession?.setRepeatingRequest(previewBuilder?.build(), null, backgroundHandler)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    /**
     * Método que cierra la cámara y las diversas sesiones asociadas a ella
     */
    protected fun closeCamera() = try {
        cameraOpenCloseLock.acquire()
        previewSession?.close()
        previewSession = null
        cameraDevice?.close()
        cameraDevice = null
        previewReader?.close()
        previewReader = null
        captureReader?.close()
        captureReader = null
    } catch (e: InterruptedException) {
        log.error(e)
        throw InterruptedCameraClosingException(e)
    } finally {
        cameraOpenCloseLock.release()
    }

    /**
     * Método que establece lo que sucede cuando se captura la imagen
     */
    protected open fun capture() {
        try {
            if (cameraDevice == null) return
            val captureBuilder = cameraDevice?.createCaptureRequest(
                CameraDevice.TEMPLATE_STILL_CAPTURE
            )?.apply {
                addTarget(captureReader?.surface)
                set(
                    CaptureRequest.CONTROL_AF_MODE,
                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE
                )
            }
            captureBuilder?.set(CaptureRequest.SCALER_CROP_REGION, zoom)
            val captureCallback = object : CameraCaptureSession.CaptureCallback() {
                override fun onCaptureCompleted(
                    session: CameraCaptureSession,
                    request: CaptureRequest,
                    result: TotalCaptureResult
                ) {
                    // Cuando captura la imágen desbloquea para que se vuelva a previsualización
                    unlockFocus()
                }
            }
            previewSession?.apply {
                stopRepeating()
                abortCaptures()
                capture(captureBuilder?.build(), captureCallback, null)
            }
        } catch (e: CameraAccessException) {
            Log.e("TAG", e.toString())
        }
    }

    // Inicia los procesos asociados
    protected fun startBackground() {
        backgroundThread = HandlerThread("Backgrounder").also { it.start() }
        backgroundHandler = Handler(backgroundThread?.looper)
    }

    /**
     * Detiene los procesos asociados al controlador de hilos
     */
    protected fun stopBackground() {
        try {
            backgroundThread?.quitSafely()
            backgroundThread?.join()
            backgroundThread = null
            backgroundHandler = null
        } catch (e: InterruptedException) {
            Log.e("BACKGROUND", e.toString())
        }
    }

    /**
     * Bloquea la imagen que esta proyectando siguiendo el ciclo de captura
     */
    protected fun lockFocus() {
        try {
            cameraRequestBuilder?.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START)
            state = CaptureState.STATE_WAITING_LOCK
            previewSession?.capture(cameraRequestBuilder?.build(), captureCallback, backgroundHandler)
        } catch (e: CameraAccessException) {
            Log.e("TAG", e.toString())
        }
    }

    /**
     * Desbloquea la imágen para que la imagen vuelva a previsualizarse
     */
    protected fun unlockFocus() {
        try {
            // Reset the auto-focus trigger
            cameraRequestBuilder?.set(
                CaptureRequest.CONTROL_AF_TRIGGER,
                CameraMetadata.CONTROL_AF_TRIGGER_CANCEL
            )

            previewSession?.capture(
                cameraRequestBuilder?.build(), captureCallback,
                backgroundHandler
            )
            state = CaptureState.STATE_PREVIEW
            previewSession?.setRepeatingRequest(cameraRequestBuilder?.build(), captureCallback, backgroundHandler)
        } catch (e: CameraAccessException) {
            Log.e("Camera2Activity", e.toString())
        }
    }

    private fun capturePicture(result: CaptureResult) {
        val afState = result.get(CaptureResult.CONTROL_AF_STATE)
        if (afState == CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED) {
            restartFocus()
        }
        if (afState == null) {
            capture()
        } else if (afState == CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED ||
            afState == CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED ||
            afState == CaptureResult.CONTROL_AF_STATE_INACTIVE
        ) {

            // CONTROL_AE_STATE can be null on some devices
            val aeState = result.get(CaptureResult.CONTROL_AE_STATE)
            if (aeState == null || aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                state = CaptureState.STATE_PICTURE_TAKEN
                capture()
            } else {
                runPrecaptureSequence()
            }
        }
    }

    private fun runPrecaptureSequence() {
        try {
            // This is how to tell the camera to trigger.
            // Tell #captureCallback to wait for the precapture sequence to be set.
            state = CaptureState.STATE_WAITING_PRECAPTURE
            previewSession?.capture(cameraRequestBuilder?.build(), captureCallback, backgroundHandler)
        } catch (e: CameraAccessException) {
            Log.e("TAG", e.toString())
        }
    }

    private fun restartFocus() {
        try {
            cameraRequestBuilder?.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_CANCEL)
            cameraRequestBuilder?.build().let {
                previewSession?.capture(it, captureCallback, backgroundHandler)
            }
            lockFocus()
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    companion object {
        private const val maxReaderCapacity = 3
    }
}
