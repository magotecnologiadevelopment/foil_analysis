package co.edu.uan.foilanalysis.usecases

import co.edu.uan.foilanalysis.model.Hole
import org.opencv.core.Mat
import org.opencv.imgproc.Imgproc
import kotlin.math.roundToInt

class AnalyzerImpl(private val config: AnalyzeConfig) : Analyzer {
    override fun findCircles(grayImage: Mat): Pair<Mat,List<Hole>> {
        val circles = Mat()
        Imgproc.HoughCircles(
            grayImage,
            circles,
            Imgproc.HOUGH_GRADIENT, 1.0,
            config.expectedRadius * (1 - config.radiusTolerance),
            config.cannyThreshold1, config.cannyThreshold2,
            (config.expectedRadius * (1 - config.radiusTolerance)).roundToInt(),
            (config.expectedRadius * (1 + config.radiusTolerance)).roundToInt()
        )
        return Pair(circles,ArrayList<Hole>())
    }
}