package co.edu.uan.foilanalysis.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import co.edu.uan.foilanalysis.fragments.SettingsRootFragment
import org.jetbrains.anko.intentFor

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager
            .beginTransaction()
            .replace(android.R.id.content, SettingsRootFragment())
            .commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
            startActivity(intentFor<CaptureActivity>())
        }
    }
}
