package co.edu.uan.foilanalysis.activities.utils

import android.util.Log
import android.util.Size
import java.lang.Long.signum
import java.util.Collections

class CompareSizeByArea : Comparator<Size> {

    override fun compare(o1: Size, o2: Size) =
        signum(o1.width.toLong() * o1.height - o2.width.toLong() * o2.height)

    companion object {
        fun chooseOptimalSize(
            choices: Array<Size>,
            textureViewSize: Size,
            maxSize: Size,
            aspectRatio: Size?
        ): Size {

            // Collect the supported resolutions that are at least as big as the preview Surface
            val bigEnough = ArrayList<Size>()
            // Collect the supported resolutions that are smaller than the preview Surface
            val notBigEnough = ArrayList<Size>()
            val w = aspectRatio?.width
            val h = aspectRatio?.height
            for (option in choices) {
                // Si es menor que el maximo y tiene la misma relación de aspecto
                if (option.width <= maxSize.width && option.height <= maxSize.height &&
                    option.height == option.width * h!! / w!!
                ) {
                    // Son mas grandes que el textureView
                    if (option.width >= textureViewSize.width && option.height >= textureViewSize.height) {
                        bigEnough.add(option)
                    } else {
                        notBigEnough.add(option)
                    }
                }
            }

            // Pick the smallest of those big enough. If there is no one big enough, pick the
            // largest of those not big enough.
            return when {
                bigEnough.size > 0 -> Collections.min(bigEnough, CompareSizeByArea())
                notBigEnough.size > 0 -> Collections.max(notBigEnough, CompareSizeByArea())
                else -> {
                    Log.e("Optimal Size", "Couldn't find any suitable preview size")
                    choices[0]
                }
            }
        }
    }
}
