package co.edu.uan.foilanalysis.activities.utils

import android.graphics.Bitmap
import java.io.File
import java.io.FileOutputStream

class ThreadB(private val syncToken: Object, val file: File, val image: Bitmap) : Thread() {
    override fun run() {
        synchronized(syncToken) {
            try {
                var output = FileOutputStream(file)
                output = FileOutputStream(file).apply {
                    image.compress(
                        Bitmap.CompressFormat.JPEG,
                        quality, output
                    )
                    flush()
                }
                syncToken.notifyAll()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
    }

    companion object {
        private const val quality = 100
    }
}
