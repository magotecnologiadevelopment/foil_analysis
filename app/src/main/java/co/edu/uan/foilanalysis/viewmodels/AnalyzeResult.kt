package co.edu.uan.foilanalysis.viewmodels

data class AnalyzeResult(val circles: Int = 0)