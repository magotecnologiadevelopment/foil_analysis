package co.edu.uan.foilanalysis.usecases

enum class ThresholdingTypes {
    NONE,
    Binary,
    Binary_Inv,
    Truncate,
    Truncate_Inv,
    Otsu,
    Adaptative_Gaussian_7,
    Adaptative_Gaussian_11,
    Adaptative_Mean_7,
    Adaptative_Mean_11,
    Otsu16
}
