package co.edu.uan.foilanalysis.usecases

import org.opencv.core.Core
import org.opencv.core.Mat
import org.opencv.core.Scalar
import org.opencv.imgproc.Imgproc

class ImageBeautifierLab : ImageBeautifier {
    override fun reduceGaussianBlur(grayRGB: Mat): Mat {
        TODO("not implemented") // To change body of created functions use File | Settings | File Templates.
    }

    override fun applyFilters(imageRGB: Mat): Mat {
        TODO("not implemented") // To change body of created functions use File | Settings | File Templates.
    }

    // TODO Quitar esta clase
    override fun applyBrightness(imageRGB: Mat, value: Double): Pair<Mat, Long> {
        val startProcessTime = System.currentTimeMillis()
        val imageLab = Mat()
        Imgproc.cvtColor(imageRGB, imageLab, Imgproc.COLOR_RGB2Lab)
        val channelList: MutableList<Mat> = arrayListOf()
        Core.split(imageLab, channelList)
        val brighted = Mat()
        Core.add(channelList[0], Scalar(value), brighted)
        channelList[0] = brighted
        val resultLab = Mat()
        Core.merge(channelList, resultLab)
        val resultImage = Mat()
        Imgproc.cvtColor(resultLab, resultImage, Imgproc.COLOR_Lab2RGB)
        val endProcessTime = System.currentTimeMillis()
        val processTime = endProcessTime - startProcessTime
        return Pair(resultImage, processTime)
    }

    override fun applySharpness(imageRGB: Mat, value: Double): Mat {
        return Mat()
    }
}
