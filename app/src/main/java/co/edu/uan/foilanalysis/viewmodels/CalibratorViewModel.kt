package co.edu.uan.foilanalysis.viewmodels

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CalibratorViewModel : ViewModel() {
    var imageToShow: MutableLiveData<Bitmap> = MutableLiveData()
}