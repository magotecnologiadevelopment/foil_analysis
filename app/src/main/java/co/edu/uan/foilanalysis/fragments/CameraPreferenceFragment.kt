package co.edu.uan.foilanalysis.fragments

import android.content.Context
import android.content.SharedPreferences
import android.graphics.ImageFormat
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.hardware.camera2.CameraMetadata
import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import co.edu.uan.foilanalysis.R
import java.util.Arrays

class CameraPreferenceFragment : PreferenceFragmentCompat() {

    private val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
        when (key) {
            "pref_camera_key" -> {
                fillCameraPreviewRes()
            }
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.pref_camera, rootKey)
        fillCameraId()
        fillCameraPreviewRes()
    }

    private fun fillCameraId() {
        val manager = activity?.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val cameraQuantity = manager.cameraIdList.size
        val entries = arrayOfNulls<CharSequence>(cameraQuantity)
        val entryValues = arrayOfNulls<CharSequence>(cameraQuantity)
        for (camera in 0 until cameraQuantity) {
            val cameraCharacteristics = manager.getCameraCharacteristics(manager.cameraIdList[camera])
            val orientation = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING)
            entries[camera] =
                    when (orientation) {
                        CameraMetadata.LENS_FACING_FRONT -> "$camera Camara frontal"
                        CameraMetadata.LENS_FACING_BACK -> "$camera Camara trasera"
                        CameraMetadata.LENS_FACING_EXTERNAL -> "$camera Camara externa"
                        else -> "No reconocida"
                    }
            entryValues[camera] = manager.cameraIdList[camera]
        }
        val cameraList = findPreference("pref_camera_key") as androidx.preference.ListPreference
        cameraList.entries = entries
        cameraList.entryValues = entryValues
        bindPreferenceSummaryToValue(Arrays.asList("pref_camera_key"))
    }

    private fun fillCameraPreviewRes() {
        val cameraId = preferenceManager.sharedPreferences.getString("pref_camera_key", "0")
        val manager = activity?.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val cameraCharacteristics = manager.getCameraCharacteristics(cameraId)
        val streamConfiguration = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
        val resolutions = streamConfiguration.getOutputSizes(ImageFormat.JPEG)
        val entries = arrayOfNulls<CharSequence>(resolutions.size)
        val entryValues = arrayOfNulls<CharSequence>(resolutions.size)
        for (resolutionIndex in 0 until resolutions.size) {
            entries[resolutionIndex] = "${resolutions[resolutionIndex].height}x${resolutions[resolutionIndex].width}"
            entryValues[resolutionIndex] =
                    "${resolutions[resolutionIndex].height}x${resolutions[resolutionIndex].width}"
        }
        val cameraList = findPreference("pref_camera_capture_res") as androidx.preference.ListPreference
        cameraList.entries = entries
        cameraList.entryValues = entryValues
        bindPreferenceSummaryToValue(Arrays.asList("pref_camera_capture_res"))
    }

    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(listener)
    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(listener)
    }
}
