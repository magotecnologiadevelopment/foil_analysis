package co.edu.uan.foilanalysis.activities.utils

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

class PermissionManager(val activity: AppCompatActivity) {

    fun checkAllPermissions(): Boolean {
        for (permission in permissions) {
            // Si no ha concedido permiso lo solicita
            if (ActivityCompat.checkSelfPermission(activity, permission.first) != PackageManager.PERMISSION_GRANTED) {
                requestPermission(permission)
                return false
            }
        }
        return true
    }

    fun checkSpecificPermission(permission: String): Boolean {
        if (ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Pair(permission, permissions.firstOrNull { it -> it.first == permission }?.second ?: 0))
            return false
        }
        return true
    }

    private fun requestPermission(permission: Pair<String, Int>) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission.first)) {
            // El usuario ha rechazado la solicitud anteriormente
            // TODO: Crear un DialogFragment propio
            ActivityCompat.requestPermissions(activity, arrayOf(permission.first), permission.second)
        } else {
            ActivityCompat.requestPermissions(activity, arrayOf(permission.first), permission.second)
        }
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
    }

    companion object {
        private val permissions =
            arrayOf(
                Manifest.permission.CAMERA to 101,
                Manifest.permission.WRITE_EXTERNAL_STORAGE to 102
            )
    }
}
