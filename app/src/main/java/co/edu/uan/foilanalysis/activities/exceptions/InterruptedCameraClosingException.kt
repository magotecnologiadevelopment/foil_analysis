package co.edu.uan.foilanalysis.activities.exceptions

class InterruptedCameraClosingException(cause: Exception) : Exception()
