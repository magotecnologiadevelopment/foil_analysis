package co.edu.uan.foilanalysis.viewmodels

data class FilterConfig(
    val brightness: Int = 0,
    val contrast: Int = 1,
    val thresholdType: String = "NONE",
    val thresholdLevel: Int = 125,
    val filterPreview: Boolean = false
)
