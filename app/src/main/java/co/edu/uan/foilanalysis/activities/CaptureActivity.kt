package co.edu.uan.foilanalysis.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageFormat
import android.graphics.Point
import android.graphics.Rect
import android.graphics.SurfaceTexture
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.hardware.camera2.CaptureRequest
import android.media.Image
import android.media.ImageReader
import android.net.Uri
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.util.Size
import android.view.MotionEvent
import android.view.Surface
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.preference.PreferenceManager
import co.edu.uan.foilanalysis.R
import co.edu.uan.foilanalysis.activities.utils.CompareSizeByArea
import co.edu.uan.foilanalysis.activities.utils.PermissionManager
import co.edu.uan.foilanalysis.extentions.jpgToBitmap
import co.edu.uan.foilanalysis.extentions.setListeners
import co.edu.uan.foilanalysis.fragments.LoadImageAlertDialog
import co.edu.uan.foilanalysis.viewmodels.CaptureViewModel
import co.edu.uan.foilanalysis.viewmodels.FilterConfig
import kotlinx.android.synthetic.main.activity_capture.*
import org.jetbrains.anko.debug
import org.jetbrains.anko.error
import org.jetbrains.anko.info
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.verbose
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import java.io.FileDescriptor
import java.util.*

import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

class CaptureActivity : Camera2Activity() {
    var timeToShow = 0L
    /**
     * ViewModel Asociado para la lógica del manejo de imagenes
     */
    private lateinit var vm: CaptureViewModel
    /**
     * Esta clase gestiona la verificación y solicitud de los permisos
     */
    private val permissionManager = PermissionManager(this)
    // Tamaños de pantalla
    private lateinit var previewSize: Size
    private lateinit var preferedSize: Size
    private lateinit var sharedPreferences: SharedPreferences
    var needResume: AtomicBoolean = AtomicBoolean(true)
    private var fingerSpacing: Float = zoomDefaultValues
    private var zoomLevel: Float = zoomDefaultValues
    private val mOpenCVCallBack = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    log.info("OpenCV loaded successfully")
                    openCVisOpen = true
                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }

    private var openCVisOpen = false
    // Métodos del Ciclo de vida
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_capture)
        // Crea y se enlaza con un ViewModel
        vm = ViewModelProviders.of(this)[CaptureViewModel::class.java]
        // Se observa la propiedad image del VM y si se cambia realiza el metodo updateImageView
        vm.imageToShow.observe(this, Observer { updateImageView(it) })
        //vm.timeToShow.observe(this, Observer { updateTime(it) })
        setButtonListeners()
    }

    private fun updateTime(it: Long) {
        simpleProgressText.text = "Demoro $it"
        simpleProgressBar.visibility = View.VISIBLE
    }

    private fun setButtonListeners() {
        // El boton de captura inicia el proceso al bloquear el foco
        btn_capture.setOnClickListener {
            previewTextu.visibility = View.VISIBLE
            image_preview.visibility = View.VISIBLE
            lockFocus()
        }
        // Abre el activity de Configuración
        btn_settings.setOnClickListener {
            startActivity(intentFor<SettingsActivity>())
        }
        btn_load.setOnClickListener {
            loadImage()
        }
        log.verbose("Se asignaron los comportamientos a los botones")
    }

    @SuppressLint("MissingPermission")
    public override fun onResume() {
        super.onResume()
        if (needResume.get()) {
            if (!OpenCVLoader.initDebug()) {
                OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mOpenCVCallBack)
            } else {
                System.loadLibrary("opencv_java3")
                log.info("OpenCV library found inside package, use it!")
                mOpenCVCallBack.onManagerConnected(LoaderCallbackInterface.SUCCESS)
            }
            // Se obtienen las preferencias propias
            getPreferences()
            startBackground()
            // Revisa los permisos
            val allPermissionsGranted = permissionManager.checkAllPermissions()
            // Inicia el trabajo de actualización de la imagen
            if (allPermissionsGranted) {
                if (!cameraOpenCloseLock.tryAcquire(timeOutTime, TimeUnit.MILLISECONDS)) {
                    log.error("Time out waiting to lock camera opening.")
                }
                // Se abre la camara
                if (openCVisOpen) {
                    if (previewTextu.isAvailable) {
                        surfaceTexture = Surface(previewTextu?.surfaceTexture)
                        openCamera(preferedSize)
                    } else {
                        previewTextu.setListeners(avalaibleFunction = { _, width, height ->
                            surfaceTexture = Surface(previewTextu?.surfaceTexture)
                            openCamera(preferedSize)
                        })
                    }
                    log.debug(message = "Se han concedido todos los permisos")
                }
            }
        }
    }

    public override fun onPause() {
        stopBackground()
        closeCamera()
        super.onPause()
    }

    override fun setUpCameraOutputs(outputSize: Size) {
        val manager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            val characteristics = manager.getCameraCharacteristics(cameraId)
            // Obtiene los tamaños soportados por la camara
            val map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
            // TODO: Revisar mejor opcion
            val largest = (map.getOutputSizes(SurfaceTexture::class.java)).maxWith(CompareSizeByArea())
            val swappedDimmensions = false
            val displaySize = Point()
            windowManager.defaultDisplay.getSize(displaySize)
            // Si las dimensiones son cambiadas, cambia los tamaños de visualización
            val rotatedPreviewWidth = outputSize.width
            val rotatedPreviewHeight = outputSize.height
            val maxPreviewWidth = displaySize.x
            val maxPreviewHeight = displaySize.y
            zoom = characteristics.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE)
            // Obtiene el mejor tamaño para visualizar
            previewSize = CompareSizeByArea.chooseOptimalSize(
                map.getOutputSizes(ImageFormat.JPEG), Size(rotatedPreviewWidth, rotatedPreviewHeight),
                Size(maxPreviewWidth, maxPreviewHeight), largest
            )
            previewSize = preferedSize
            // Crea un receptor del tamaño seleccionado  y formato YUV para tratar la imagen
            previewReader = ImageReader.newInstance(
                previewSize.width,
                previewSize.height,
                ImageFormat.YUV_420_888,
                maxReaderCapacity
            )
            // Le asigna respuesta al receptor
            previewReader?.setOnImageAvailableListener(
                { reader ->
                    val image = reader.acquireNextImage()
                    processPreview(image)
                    image.close()
                },
                backgroundHandler
            )
            // Crea un receptor del tamaño seleccionado  y formato YUV para cuando se tome  la imagen
            captureReader = ImageReader.newInstance(
                preferedSize.width,
                preferedSize.height,
                ImageFormat.JPEG,
                maxReaderCapacity
            )
            captureReader?.setOnImageAvailableListener(
                { reader ->
                    val image = reader.acquireNextImage()
                    // loadImage()
                    image.close()
                },
                backgroundHandler
            )
            surfaces = Arrays.asList(captureReader?.surface, previewReader?.surface, surfaceTexture)
        } catch (e: CameraAccessException) {
            log.error(e.printStackTrace())
        }
    }

    private fun loadImage() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        needResume.getAndSet(false)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "*/*"
        startActivityForResult(Intent.createChooser(intent, "Select a Photo "), 153)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 153 && resultCode == Activity.RESULT_OK && data != null) {
            val uri: Uri = data.data
            val parcelFileDescriptor: ParcelFileDescriptor = contentResolver.openFileDescriptor(uri, "r")
            val fileDescriptor: FileDescriptor = parcelFileDescriptor.fileDescriptor
            val image: Bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor)
            parcelFileDescriptor.close()
            val fragment = LoadImageAlertDialog()
            fragment.arguments = Bundle().apply { putParcelable("image", image) }
            fragment.show(supportFragmentManager, "Desde Archivo")
        }
    }

    fun analyzeAndPost(image: Bitmap) {
        //simpleProgress.visibility = View.VISIBLE
        //simpleProgressBar.isIndeterminate = true
        //simpleProgressText.text = "Analizando"
        // TODO:Crear el activity para la recepción de los resultados
        updateImageView(vm.analyze(image))
        //updateTime(vm.timeToShow.value!!)
        this.needResume.getAndSet(false)
        // startActivity(intentFor<ResultActivity>("image" to vm.imageToShow.value))
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            onResume()
        }
    }

    override fun processCapture(image: Image?) {
        super.processCapture(image)
        if (image != null) {
            val capturedImage = image.jpgToBitmap()
            val fragment = LoadImageAlertDialog()
            fragment.arguments = Bundle().apply { putParcelable("image", capturedImage) }
            fragment.show(supportFragmentManager, "Desde Captura")
        }
    }

    override fun processPreview(image: Image?) {
        super.processPreview(image)
        if (image != null && openCVisOpen) {
            runOnUiThread {
                previewTextu.visibility = View.INVISIBLE
            }
            vm.processPreview(imageFromReader = image)
        }
    }

    // Metodo disparado por el cambio de la imagen en el VM
    private fun updateImageView(newImage: Bitmap?) {
        if (newImage != null) {
            image_preview.setImageBitmap(newImage)
        }
    }

    private fun getPreferences() {
        // Obtiene las preferencias
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        // Esta es la resolución de imagen en un formato $$$x$$$
        val prefSizeResize: String = sharedPreferences.getString("pref_camera_capture_res", "640x480") ?: "640x480"
        val brightness = sharedPreferences.getInt("pref_brightness", 0)
        val contrast = sharedPreferences.getInt("pref_contrast", 0) + 1
        val threshholdLevel = sharedPreferences.getInt("pref_threshold_level", 0)
        val threshholdType = sharedPreferences.getString("pref_threshold_type", "NONE") ?: "NONE"
        val filterPreview = sharedPreferences.getBoolean("pref_filter_preview", false)
        val configFromPrefs = FilterConfig(
            brightness = brightness,
            contrast = contrast,
            thresholdLevel = threshholdLevel,
            thresholdType = threshholdType,
            filterPreview = filterPreview
        )
        vm.setupImageProcesses(configFromPrefs, this)
        val width = Integer.parseInt(prefSizeResize.substring(0, prefSizeResize.lastIndexOf("x")))
        val height = Integer.parseInt(prefSizeResize.substring(prefSizeResize.lastIndexOf("x") + 1))
        preferedSize = Size(width, height)
        // El ID de la camara
        cameraId = sharedPreferences.getString("cameraId", "0") ?: "0"
    }

    @Suppress("ComplexMethod")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val manager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val characteristics = manager.getCameraCharacteristics(cameraId)
        val maxZoom: Float = (characteristics.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM)) * 10
        val m: Rect = characteristics.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE)
        val action = event?.action
        val currentFingerSpacing: Float
        if (event?.pointerCount!! > 1) {
            // Se es multitouch realiza el cambio de zoom
            currentFingerSpacing = getFingerSpacing(event)
            if (fingerSpacing != zoomDefaultValues) {
                if (currentFingerSpacing > fingerSpacing && maxZoom > zoomLevel) {
                    zoomLevel++
                } else if (currentFingerSpacing < fingerSpacing && zoomLevel > 1) {
                    zoomLevel--
                }
                val minW: Int = (m.width() / maxZoom).toInt()
                val minH = (m.height() / maxZoom).toInt()
                val difW = m.width() - minW
                val difH = m.height() - minH
                var cropW = difW / percentDiv * zoomLevel.toInt()
                var cropH = difH / percentDiv * zoomLevel.toInt()
                cropW -= zoomStep
                cropH -= zoomStep
                zoom = Rect(cropW, cropH, m.width() - cropW, m.height() - cropH)
                cameraRequestBuilder?.set(CaptureRequest.SCALER_CROP_REGION, zoom)
            }
            fingerSpacing = currentFingerSpacing
        } else {
            if (action == MotionEvent.ACTION_UP) {
                // stopBackground()
                // Si es un solo click
                // lockFocus()
            }
        }
        try {
            previewSession?.setRepeatingRequest(cameraRequestBuilder?.build(), null, backgroundHandler)
        } catch (e: CameraAccessException) {
            log.error(e.printStackTrace())
        }
        return true
    }

    private fun getFingerSpacing(event: MotionEvent): Float {
        val x: Double = event.getX(0) - event.getX(1).toDouble()
        val y: Double = event.getY(0) - event.getY(1).toDouble()
        return Math.sqrt(x * x + y * y).toFloat()
    }

    companion object {
        private const val zoomStep = 3
        private const val percentDiv = 100
        private const val timeOutTime: Long = 2500
        private const val maxReaderCapacity = 3
        private const val zoomDefaultValues = 0.0F
    }
}
