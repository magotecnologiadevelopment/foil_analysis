package co.edu.uan.foilanalysis.activities.utils

import android.graphics.Bitmap
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

internal class BitmapSaver(
    /**
     * The JPEG image
     */
    private val image: Bitmap,

    /**
     * The file we save the image into.
     */
    private val file: File
) : Runnable {

    override fun run() {
        var output = FileOutputStream(file)
        try {
            output = FileOutputStream(file).apply {
                image.compress(Bitmap.CompressFormat.JPEG, quality, output)
                output.flush()
            }
        } catch (e: IOException) {
            Log.e(TAG, e.toString())
        } finally {
            output.let { outputStream ->
                try {
                    outputStream.close()
                } catch (e: IOException) {
                    Log.e(TAG, e.toString())
                }
            }
        }
    }

    companion object {
        /**
         * Tag for the [Log].
         */
        private const val TAG = "ImageSaver"
        private const val quality = 100
    }
}
