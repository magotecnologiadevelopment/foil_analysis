package co.edu.uan.foilanalysis.usecases

data class AnalyzeConfig(
    val cannyThreshold1: Double,
    val cannyThreshold2: Double,
    val expectedRadius: Double,
    val radiusTolerance: Double
)
