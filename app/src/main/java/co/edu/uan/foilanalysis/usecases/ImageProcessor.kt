package co.edu.uan.foilanalysis.usecases

import org.opencv.core.Mat

interface ImageProcessor {
    fun turnBlack(imageRGB: Mat): Mat
    fun thresholding(imageGrayScale: Mat, thresholdType: String, thesholdLevel: Double): Mat
    fun segmentation(imageRGB: Mat): Mat
    fun findContours(blackImage: Mat): Mat
    fun findContour(blackImage: Mat, contourId: Int): Mat
    fun findEdgesByCanny(blackImage: Mat, thr1: Double, thr2: Double): Mat
}
