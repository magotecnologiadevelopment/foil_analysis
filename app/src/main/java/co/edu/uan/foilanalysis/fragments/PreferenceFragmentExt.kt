package co.edu.uan.foilanalysis.fragments

import android.preference.ListPreference
import android.preference.PreferenceManager
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat

fun PreferenceFragmentCompat.bindPreferenceSummaryToValue(preferences: List<String>) {

    val sBindPreferenceSummaryToValueListener = Preference.OnPreferenceChangeListener { preference, value ->
        val stringValue = value.toString()
        when (preference) {
            is ListPreference -> {
                val index = preference.findIndexOfValue(stringValue)
                preference.setSummary(
                    if (index >= 0)
                        preference.entryValues[index]
                    else
                        null
                )
            }
        }
        if (preference is ListPreference) {
            // For list preferences, look up the correct display value in
            // the preference's 'entries' list.

            // Set the summary to reflect the new value.
        } else {
            // For all other preferences, set the summary to the value's
            // simple string representation.
            preference.summary = stringValue
        }
        true
    }

    // Set the listener to watch for value changes.
    for (key in preferences) {
        val preference = findPreference(key)
        preference.onPreferenceChangeListener = sBindPreferenceSummaryToValueListener
        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(
            preference,
            PreferenceManager
                .getDefaultSharedPreferences(preference.context)
                .getString(preference.key, "")
        )
    }
}
