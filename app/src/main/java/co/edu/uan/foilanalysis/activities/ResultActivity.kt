package co.edu.uan.foilanalysis.activities

import android.graphics.Bitmap
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import co.edu.uan.foilanalysis.R

class ResultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        if (intent != null) {
            if (intent.extras.containsKey("image")) {
                intent.extras.getSerializable("serializable") as Bitmap
            }
        }
    }
}