package co.edu.uan.foilanalysis.usecases

import org.opencv.core.Mat
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import co.edu.uan.foilanalysis.model.Hole
import org.opencv.android.Utils
import org.opencv.core.Core
import org.opencv.core.Core.NORM_MINMAX
import org.opencv.core.CvType
import org.opencv.core.Point
import org.opencv.imgproc.Imgproc
import java.io.FileNotFoundException
import java.io.IOException
import org.opencv.imgcodecs.Imgcodecs.CV_LOAD_IMAGE_COLOR
import org.opencv.core.Scalar
import org.jetbrains.anko.AnkoLogger
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.graphics.Canvas
import android.graphics.Paint
import org.jetbrains.anko.debug


class AnalyzerImplSOFA(val context: Context) : Analyzer, AnkoLogger {
    override fun findCircles(grayImage: Mat): Pair<Mat, List<Hole>> {
        return findCenterPointbyImageCorrelation(grayImage)
    }

    @Throws(FileNotFoundException::class, IOException::class)
    fun findCenterPointbyImageCorrelation(image: Mat): Pair<Mat, List<Hole>> {
        var center: Point? = null
        // ImgtoCorr = CorrelationImage()
        val holeTemplate =
            Utils.loadResource(context, co.edu.uan.foilanalysis.R.drawable.hole_mask, CV_LOAD_IMAGE_COLOR)
        // Imgproc.resize(holeTemplate,holeTemplate, Size(14.0,14.0))
        val result = Mat()
        Imgproc.cvtColor(holeTemplate, holeTemplate, Imgproc.COLOR_RGB2GRAY)
        holeTemplate.convertTo(result, CvType.CV_8U)
        val matchMethod = Imgproc.TM_CCORR_NORMED
        Imgproc.matchTemplate(image, holeTemplate, result, matchMethod)
        Core.normalize(result, result, 0.0, 1.0, NORM_MINMAX, -1, Mat())
        val matchLoc: Point
        val mmr = Core.minMaxLoc(result)
        matchLoc = if (matchMethod === Imgproc.TM_SQDIFF || matchMethod === Imgproc.TM_SQDIFF_NORMED) {
            mmr.minLoc
        } else {
            mmr.maxLoc
        }
        val holes = mutableListOf<Hole>()
        val totalMat = Mat(image.rows(), image.cols(), CvType.CV_8UC3)
        //Imgproc.rectangle(image, matchLoc, Point(matchLoc.x + holeTemplate.cols(), matchLoc.y + holeTemplate.rows()), Scalar(0.0, 200.0, 0.0), 2, 8)
        val firstCenter = Point(matchLoc.x + (holeTemplate.cols() / 2), matchLoc.y + (holeTemplate.cols() / 2))
        var imageBit: Bitmap = Bitmap.createBitmap(image.cols(), image.rows(), Bitmap.Config.ARGB_8888)
        Utils.matToBitmap(image, imageBit)

        debug("Encontrado el primer punto en: ${firstCenter.x} : $firstCenter.y" )
        holes.addAll(
            Replicator().diagonalCenterReplication(
                imageBit,
                Replicator.Companion.VerticalDirection.DOWN,
                firstCenter
            )
        )
        holes.addAll(
            Replicator().diagonalCenterReplication(
                imageBit,
                Replicator.Companion.VerticalDirection.UP,
                firstCenter
            )
        )
        Imgproc.cvtColor(image, image, Imgproc.COLOR_GRAY2RGB)

        for (hole in holes) {
            for (inPoint in hole.innerCounture.points) {
                if (inPoint.x.toInt() > 0 && inPoint.x.toInt() < imageBit.width &&
                    inPoint.y > 0 && inPoint.y < imageBit.height) {
                    imageBit.setPixel(inPoint.x.toInt(), inPoint.y.toInt(), Color.GREEN)
                }
            }

            for (outPoint in hole.outerCounture.points) {
                if (outPoint.x.toInt() > 0 && outPoint.x.toInt() < imageBit.width && outPoint.y > 0 && outPoint.y < imageBit.height) {
                    imageBit.setPixel(outPoint.x.toInt(), outPoint.y.toInt(), Color.BLUE)
                }
            }
            if (hole.center.x > 0 && hole.center.x < imageBit.width && hole.center.y > 0 && hole.center.y < imageBit.height) {
                for (i in 0..2){
                    imageBit.setPixel(hole.center.x.toInt()+i, hole.center.y.toInt()+i, Color.RED)
                    imageBit.setPixel(hole.center.x.toInt()-i, hole.center.y.toInt()+i, Color.RED)
                    imageBit.setPixel(hole.center.x.toInt()-i, hole.center.y.toInt()-i, Color.RED)
                    imageBit.setPixel(hole.center.x.toInt()+i, hole.center.y.toInt()-i, Color.RED)
                    imageBit.setPixel(hole.center.x.toInt(), hole.center.y.toInt(), Color.RED)
                }

            }
            if (hole.outerCounture.initPoint.x > 0 && hole.outerCounture.initPoint.x < imageBit.width && hole.outerCounture.initPoint.y > 0 && hole.outerCounture.initPoint.y < imageBit.height) {
                for (i in 0..2){
                    imageBit.setPixel(hole.outerCounture.initPoint.x.toInt()+i, hole.outerCounture.initPoint.y.toInt()+i, Color.YELLOW)
                    imageBit.setPixel(hole.outerCounture.initPoint.x.toInt()-i, hole.outerCounture.initPoint.y.toInt()+i, Color.YELLOW)
                    imageBit.setPixel(hole.outerCounture.initPoint.x.toInt()-i, hole.outerCounture.initPoint.y.toInt()-i, Color.YELLOW)
                    imageBit.setPixel(hole.outerCounture.initPoint.x.toInt()+i, hole.outerCounture.initPoint.y.toInt()-i, Color.YELLOW)
                    imageBit.setPixel(hole.outerCounture.initPoint.x.toInt(), hole.outerCounture.initPoint.y.toInt(), Color.YELLOW)
                }
            }

        }

        Utils.bitmapToMat(imageBit, totalMat)
        return Pair(totalMat, holes)
    }
}
