package co.edu.uan.foilanalysis.managers

enum class CaptureState {
    STATE_PREVIEW,
    STATE_WAITING_LOCK,
    STATE_WAITING_PRECAPTURE,
    STATE_WAITING_NON_PRECAPTURE,
    STATE_PICTURE_TAKEN
}
