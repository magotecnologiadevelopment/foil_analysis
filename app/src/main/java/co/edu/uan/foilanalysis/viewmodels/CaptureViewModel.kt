package co.edu.uan.foilanalysis.viewmodels

import android.content.Context
import android.graphics.Bitmap
import android.media.Image
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.edu.uan.foilanalysis.extentions.toMat
import co.edu.uan.foilanalysis.usecases.AnalyzeConfig
import co.edu.uan.foilanalysis.usecases.Analyzer
import co.edu.uan.foilanalysis.usecases.AnalyzerImpl
import co.edu.uan.foilanalysis.usecases.AnalyzerImplSOFA
import co.edu.uan.foilanalysis.usecases.ImageBeautifier
import co.edu.uan.foilanalysis.usecases.ImageBeautifierImpl
import co.edu.uan.foilanalysis.usecases.ImageProcessor
import co.edu.uan.foilanalysis.usecases.ImageProcessorImpl
import org.opencv.android.Utils
import org.opencv.core.Core
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Point
import org.opencv.core.Rect
import org.opencv.core.Scalar
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc

class CaptureViewModel : ViewModel() {
    var imageToShow: MutableLiveData<Bitmap> = MutableLiveData()
    var timeToShow: MutableLiveData<Long> = MutableLiveData<Long>().apply { setValue(0L) }
    private lateinit var beautifier: ImageBeautifier
    private lateinit var processor: ImageProcessor
    private lateinit var analyzer: Analyzer
    private var filterPreview: Boolean = false
    /**
     ** Metodo que realiza el procesado de una imagen [imageFromReader] de tipo YUV
     */
    fun processPreview(imageFromReader: Image) {
        val foundImage = imageFromReader.toMat()
        val coloredMat = Mat(Size(foundImage.cols().toDouble(), foundImage.rows().toDouble()), CvType.CV_8UC3)
        Imgproc.cvtColor(foundImage, coloredMat, Imgproc.COLOR_YUV2RGB_I420)
        // Aplica brillo, contraste y afilado
        var beautifiedImage = beautifier.applyFilters(coloredMat)
        Core.rotate(beautifiedImage, beautifiedImage, Core.ROTATE_90_CLOCKWISE)
        if (filterPreview) {
            // Si esta seleccionado aplica segmentación
            beautifiedImage = processor.segmentation(beautifiedImage)
        }
        val bitmap = Bitmap.createBitmap(beautifiedImage.cols(), beautifiedImage.rows(), Bitmap.Config.ARGB_8888)
        Utils.matToBitmap(beautifiedImage, bitmap)
        imageToShow.postValue(bitmap)
    }

    fun analyze(imagejpg: Bitmap): Bitmap {
        var mat = Mat()
        Utils.bitmapToMat(imagejpg, mat)

        val rect = Rect(Point(0.0, 0.0), Point((mat.cols()) / 4.toDouble(), (mat.rows()) /4.toDouble()))
        mat = mat.submat(rect)

        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGBA2RGB)
        val black = processor.turnBlack(mat)
        val segmentated = processor.segmentation(mat)
        var aftersegment = segmentated

        aftersegment = beautifier.reduceGaussianBlur(aftersegment)
        Imgproc.cvtColor(aftersegment, aftersegment, Imgproc.COLOR_GRAY2RGB)
        val kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, Size(3.0, 3.0))
        Imgproc.morphologyEx(
            aftersegment,
            aftersegment,
            Imgproc.MORPH_BLACKHAT,
            kernel,
            Point(1.0, 1.0),
            25
        )
        Imgproc.threshold(aftersegment, aftersegment, 254.0, maximumValue, Imgproc.THRESH_BINARY_INV)
        // black.convertTo(black,CvType.CV_16SC1)
        // Imgproc.Canny(segmentated, segmentated, cannyThreshold1, cannyThreshold2, cannyAperture, true)
        // var countours = processor.findEdgesByCanny(black,cannyThreshold1, cannyThreshold2)
        // black.convertTo(black,CvType.CV_8UC1)
        // countours.convertTo(countours,CvType.CV_8UC1)
        Imgproc.cvtColor(aftersegment, aftersegment, Imgproc.COLOR_RGB2GRAY)
        val countours = processor.findContours(aftersegment)

        Imgproc.cvtColor(aftersegment, aftersegment, Imgproc.COLOR_GRAY2RGB)
        val postBitmap: Bitmap = Bitmap.createBitmap(aftersegment.cols(), aftersegment.rows(), Bitmap.Config.ARGB_8888)
        // Imgproc.cvtColor(circles,circles,Imgproc.COLOR_GRAY2RGB)
        Utils.matToBitmap(aftersegment, postBitmap)
        //imageToShow.postValue(postBitmap)
        Imgproc.cvtColor(aftersegment, aftersegment, Imgproc.COLOR_RGB2GRAY)
        val time1 = System.currentTimeMillis()
        val circles = analyzer.findCircles(aftersegment)
        val time2 = System.currentTimeMillis()
        val findCirclesTime = time2 - time1
        //timeToShow.postValue(findCirclesTime)
        //var result = AnalyzeResult(circles.cols())
        val returnBitmap: Bitmap =
            Bitmap.createBitmap(circles.first.cols(), circles.first.rows(), Bitmap.Config.ARGB_8888)
        Imgproc.cvtColor(black, black, Imgproc.COLOR_GRAY2RGB)
        Utils.matToBitmap(circles.first, returnBitmap)
        return returnBitmap
    }

    fun setupImageProcesses(filterConfig: FilterConfig, context: Context) {
        filterPreview = filterConfig.filterPreview
        beautifier = ImageBeautifierImpl(
            brightness = filterConfig.brightness.toDouble(),
            contrast = filterConfig.contrast.toDouble()
        )
        processor = ImageProcessorImpl(
            thresholdType = filterConfig.thresholdType,
            thesholdLevel = filterConfig.thresholdLevel.toDouble()
        )
        analyzer = AnalyzerImpl(AnalyzeConfig(cannyThreshold1, cannyThreshold2, 10.0, 0.6))
        analyzer = AnalyzerImplSOFA(context)
    }

    fun analyze(): Bitmap? {
        if (imageToShow.value != null) {
            return analyze(imageToShow.value!!)
        }
        return null
    }

    companion object {
        // TODO Remover o calcular los valores de umbral para Canny
        private const val cannyThreshold1 = 50.0
        private const val cannyThreshold2 = 13.0
        private const val cannyAperture = 3
        private val circleColor = Scalar(0.0, 0.0, 255.0)
        private const val maximumValue = 255.0
    }
}
