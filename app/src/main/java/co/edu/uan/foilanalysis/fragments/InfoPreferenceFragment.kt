package co.edu.uan.foilanalysis.fragments

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import co.edu.uan.foilanalysis.R
import java.util.Arrays

/**
 * This fragment shows notification preferences only. It is used when the
 * activity is showing a two-pane settings UI.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
class InfoPreferenceFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(p0: Bundle?, p1: String?) {
        setPreferencesFromResource(R.xml.pref_info, p1)
        bindPreferenceSummaryToValue(Arrays.asList("notifications_new_message_ringtone"))
    }
}
