package co.edu.uan.foilanalysis.usecases

import org.opencv.core.Mat

interface ImageBeautifier {

    fun applyBrightness(imageRGB: Mat, value: Double): Pair<Mat, Long>
    fun applySharpness(imageRGB: Mat, value: Double): Mat
    fun applyFilters(imageRGB: Mat): Mat
    fun reduceGaussianBlur(grayRGB: Mat): Mat
}
