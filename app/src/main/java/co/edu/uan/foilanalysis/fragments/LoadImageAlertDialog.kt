package co.edu.uan.foilanalysis.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDialogFragment
import co.edu.uan.foilanalysis.R
import co.edu.uan.foilanalysis.activities.CaptureActivity
import co.edu.uan.foilanalysis.viewmodels.CaptureViewModel
import kotlinx.android.synthetic.main.alertdialog_load_image_from_file.view.*

class LoadImageAlertDialog : AppCompatDialogFragment() {
    lateinit var image: Bitmap
    lateinit var vm: CaptureViewModel

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        image = arguments?.get("image") as Bitmap
        val activity = activity
        val builder = AlertDialog.Builder(activity)
        val messageView = View.inflate(activity, R.layout.alertdialog_load_image_from_file, null)
        builder.setTitle("Imagen obtenida desde el sistema de archivos")
            .setCancelable(true)
            .setView(messageView)
        messageView.imageFromFile.setImageBitmap(image)
        val alertDialog = builder.create()
        messageView.btn_analyzeFromImage.setOnClickListener {
            val originActivity = (this.activity as CaptureActivity)
            // messageView.imageFromFile.setImageBitmap(originActivity.vm.analyze(image))
            // originActivity.updateImageView(originActivity.vm.analyze(image))
            // originActivity.updateTime(originActivity.vm.timeToShow.value!!)
            originActivity.analyzeAndPost(image)
            alertDialog.dismiss()
        }

        messageView.btn_closeFromImage.setOnClickListener {
            val originActivity = (this.activity as CaptureActivity)
            originActivity.needResume.getAndSet(true)
            alertDialog.dismiss()
        }

        // Crea el dialogo
        return alertDialog
    }
}