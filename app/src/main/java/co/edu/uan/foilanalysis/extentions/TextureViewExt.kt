package co.edu.uan.foilanalysis.extentions

import android.graphics.Matrix
import android.graphics.RectF
import android.graphics.SurfaceTexture
import android.util.Size
import android.view.Surface
import android.view.TextureView

fun TextureView.setListeners(

    sizeChangedFuntion: (surface: SurfaceTexture?, width: Int, height: Int) -> Unit = { _, _, _ -> },
    updatedFunction: (surface: SurfaceTexture?) -> Unit = { },
    destroyedFuntion: (surface: SurfaceTexture?) -> Boolean = { true },
    avalaibleFunction: (surface: SurfaceTexture?, width: Int, height: Int) -> Unit = { _, _, _ -> }
) {

    val surfaceListener = object : TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture?, width: Int, height: Int) {
            sizeChangedFuntion(surface, width, height)
        }

        override fun onSurfaceTextureUpdated(surface: SurfaceTexture?) {
            updatedFunction(surface)
        }

        override fun onSurfaceTextureDestroyed(surface: SurfaceTexture?): Boolean {
            return destroyedFuntion(surface)
        }

        override fun onSurfaceTextureAvailable(surface: SurfaceTexture?, width: Int, height: Int) {
            avalaibleFunction(surface, width, height)
        }
    }
    this.surfaceTextureListener = surfaceListener
}

fun TextureView.configureTransform(previewSize: Size, rotation: Int) {
    val rotate180 = 180f
    val rotate90 = 90f
    val matrix = Matrix()
    val viewRect = RectF(0f, 0f, this.width.toFloat(), this.height.toFloat())
    val bufferRect = RectF(0f, 0f, previewSize.width.toFloat(), previewSize.height.toFloat())
    val centerX = viewRect.centerX()
    val centerY = viewRect.centerY()
    if (rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) {
        bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY())
        val scale =
            Math.max(width.toFloat() / previewSize.width.toFloat(), height.toFloat() / previewSize.height.toFloat())
        with(matrix) {
            setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL)
            postScale(scale, scale, centerX, centerY)
            postRotate(rotate90 * (rotation - 2).toFloat(), centerX, centerY)
        }
    } else if (rotation == Surface.ROTATION_180) {
        matrix.postRotate(rotate180, centerX, centerY)
    }
    matrix.postRotate(0f, centerX, centerY)
    this.setTransform(matrix)
}
