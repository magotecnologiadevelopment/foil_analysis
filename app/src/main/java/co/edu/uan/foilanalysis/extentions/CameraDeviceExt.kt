package co.edu.uan.foilanalysis.extentions

import android.hardware.camera2.CameraCaptureSession
import android.hardware.camera2.CameraDevice
import android.os.Handler
import android.view.Surface
import co.edu.uan.foilanalysis.activities.Camera2Activity

fun getBasicStateResponse(
    activity: Camera2Activity,
    openedFunction: (camera: CameraDevice) -> Unit = { it -> }
): CameraDevice.StateCallback {
    return object : CameraDevice.StateCallback() {
        override fun onDisconnected(camera: CameraDevice) {
            camera.close()
            activity.cameraOpenCloseLock.release()
            activity.cameraDevice = null
        }

        override fun onError(camera: CameraDevice, error: Int) {
            onDisconnected(camera = camera)
        }

        override fun onOpened(camera: CameraDevice) {
            activity.cameraOpenCloseLock.release()
            activity.cameraDevice = camera
            openedFunction(camera)
        }
    }
}

fun CameraDevice.createSession(
    onConfigFunction: (cameraCaptureSession: CameraCaptureSession) -> Unit = { cameraCaptureSession -> },
    onConfigFailed: (cameraCaptureSession: CameraCaptureSession) -> Unit = { cameraCaptureSession -> },
    surfaces: MutableList<Surface>,
    handler: Handler?
) {
    val callback = object : CameraCaptureSession.StateCallback() {
        override fun onConfigured(cameraCaptureSession: CameraCaptureSession) {
            onConfigFunction(cameraCaptureSession)
        }

        override fun onConfigureFailed(cameraCaptureSession: CameraCaptureSession) {
            onConfigFailed(cameraCaptureSession)
        }
    }
    this.createCaptureSession(surfaces, callback, handler)
}
