package co.edu.uan.foilanalysis.usecases

import co.edu.uan.foilanalysis.usecases.exceptions.UnknownThresholdTypeException
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.MatOfPoint
import org.opencv.core.Point
import org.opencv.core.Rect
import org.opencv.core.Scalar
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc

class ImageProcessorImpl(val thesholdLevel: Double, val thresholdType: String) : ImageProcessor {

    override fun turnBlack(imageRGB: Mat): Mat {
        val gray = Mat()
        Imgproc.cvtColor(imageRGB, gray, Imgproc.COLOR_RGB2GRAY)
        return gray
    }

    /**
     * Método que realiza umbralización
     * @param imageGrayScale Image a ser umbralizada en escala de grises
     * @param thresholdType Nombre del tipo de Umbralización
     * @param thresholdLevel parametros de la umbralización
     * @return imagen umbralizada en formato de escala de grises
     * Se suprime el análisis estatico debido a que su complejidad
     * solamente radica en la cantidad de casos del When
     */
    @Suppress("ComplexMethod")
    override fun thresholding(imageGrayScale: Mat, thresholdType: String, thresholdLevel: Double): Mat {
        var threshMat = Mat(Size(imageGrayScale.cols().toDouble(), imageGrayScale.rows().toDouble()), CvType.CV_8UC1)
        when (thresholdType) {
            ThresholdingTypes.NONE.name -> threshMat = imageGrayScale
            ThresholdingTypes.Binary.name ->
                Imgproc.threshold(imageGrayScale, threshMat, thresholdLevel, maxColorValue, Imgproc.THRESH_BINARY)
            ThresholdingTypes.Binary_Inv.name ->
                Imgproc.threshold(imageGrayScale, threshMat, thresholdLevel, maxColorValue, Imgproc.THRESH_BINARY_INV)
            ThresholdingTypes.Truncate.name ->
                Imgproc.threshold(imageGrayScale, threshMat, thresholdLevel, maxColorValue, Imgproc.THRESH_TRUNC)
            ThresholdingTypes.Truncate_Inv.name ->
                Imgproc.threshold(imageGrayScale, threshMat, thresholdLevel, maxColorValue, Imgproc.THRESH_BINARY_INV)
            ThresholdingTypes.Otsu.name ->
                Imgproc.threshold(imageGrayScale, threshMat, thresholdLevel, maxColorValue, Imgproc.THRESH_OTSU)
            ThresholdingTypes.Adaptative_Mean_11.name ->
                Imgproc.adaptiveThreshold(
                    imageGrayScale,
                    threshMat,
                    maxColorValue,
                    Imgproc.ADAPTIVE_THRESH_MEAN_C,
                    Imgproc.THRESH_BINARY,
                    largeBlockSize,
                    thresholdLevel
                )
            ThresholdingTypes.Adaptative_Mean_7.name ->
                Imgproc.adaptiveThreshold(
                    imageGrayScale,
                    threshMat,
                    maxColorValue,
                    Imgproc.ADAPTIVE_THRESH_MEAN_C,
                    Imgproc.THRESH_BINARY,
                    mediumBlockSize,
                    thresholdLevel
                )
            ThresholdingTypes.Adaptative_Gaussian_7.name ->
                Imgproc.adaptiveThreshold(
                    imageGrayScale,
                    threshMat,
                    maxColorValue,
                    Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                    Imgproc.THRESH_BINARY,
                    mediumBlockSize,
                    thresholdLevel
                )
            ThresholdingTypes.Adaptative_Gaussian_11.name ->
                Imgproc.adaptiveThreshold(
                    imageGrayScale,
                    threshMat,
                    maxColorValue,
                    Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                    Imgproc.THRESH_BINARY,
                    largeBlockSize,
                    thresholdLevel
                )
            ThresholdingTypes.Otsu16.name -> {
                val splittedImages = Array(16) { i -> Mat() }
                for (i in 0 until 16) {
                    val rect = Rect(
                        Point((imageGrayScale.cols() / 4).toDouble()*(i % 4.0), (imageGrayScale.rows() / 4).toDouble()*(i / 4.0)),
                        Point((imageGrayScale.cols() / 4).toDouble()*((i+1) % 4.0), (imageGrayScale.rows() / 4).toDouble()*((i+1) / 4))
                    )
                    imageGrayScale.adjustROI(0,0,0,0
                    )
                    splittedImages[i] = imageGrayScale.submat(rect)
                    splittedImages[i] = thresholding(splittedImages[i], ThresholdingTypes.Otsu.name, thresholdLevel)
                    splittedImages[i].copyTo(imageGrayScale)
                }
            }

            else ->
                throw UnknownThresholdTypeException()
        }
        return threshMat
    }

    override fun segmentation(imageRGB: Mat): Mat {
        var segmentatedImage = turnBlack(imageRGB = imageRGB)
        segmentatedImage = thresholding(
            imageGrayScale = segmentatedImage,
            thresholdType = thresholdType,
            thresholdLevel = thesholdLevel
        )
        return segmentatedImage
    }

    override fun findEdgesByCanny(blackImage: Mat, thr1: Double, thr2: Double): Mat {
        val edges = Mat()
        Imgproc.Canny(blackImage, blackImage, edges, thr1, thr2)
        return edges
    }

    override fun findContours(blackImage: Mat): Mat {
        val detectedCountours = mutableListOf<MatOfPoint>()
        val hierarchy = Mat()

        Imgproc.findContours(blackImage, detectedCountours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_NONE)
        val countures = Mat(blackImage.rows(), blackImage.cols(), CvType.CV_8UC3)
        Imgproc.drawContours(countures, detectedCountours, -1, Scalar(maxColorValue, 0.0, 0.0), thickness, 1, hierarchy)
        // Imgproc.drawContours(countures, detectedCountours, -1, Scalar(maxColorValue, 0.0, 0.0), thickness)
        return countures
    }

    override fun findContour(blackImage: Mat, contourId: Int): Mat {
        val detectedCountours = mutableListOf<MatOfPoint>()
        val hierarchy = Mat()
        Imgproc.findContours(blackImage, detectedCountours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_NONE)
        val countures = Mat(blackImage.rows(), blackImage.cols(), CvType.CV_8UC4)
        Imgproc.drawContours(countures, detectedCountours, contourId, Scalar(maxColorValue, 0.0, 0.0), thickness)
        return countures
    }

    companion object {
        private const val maxColorValue = 255.0
        private const val mediumBlockSize = 7
        private const val largeBlockSize = 11
        private const val thickness = 2
    }
}
