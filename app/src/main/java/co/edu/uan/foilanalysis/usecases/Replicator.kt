package co.edu.uan.foilanalysis.usecases

import android.graphics.Bitmap
import android.graphics.Color
import co.edu.uan.foilanalysis.model.FoilCounture
import co.edu.uan.foilanalysis.model.Hole
import org.jetbrains.anko.AnkoLogger
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.MatOfPoint
import org.opencv.core.Point
import org.opencv.imgproc.Imgproc
import java.util.*
import kotlin.math.atan
import kotlin.math.cos
import kotlin.math.roundToInt
import kotlin.math.sin

class Replicator : AnkoLogger {

    var desviation=0.0

    fun diagonalCenterReplication(imageToCheck: Bitmap, direction: VerticalDirection, firstCenter: Point): List<Hole> {
        val centers = mutableListOf<Point>()
        val holesbyImage = ArrayList<Hole>()
        val down = imageToCheck.height - minimunRadius
        var newPoint = when (direction) {
            VerticalDirection.UP // Diagonal Hacia Arriba : Cuando es hacia arriba se calcula el siguiente en diagonal
            -> {
                Point(
                    firstCenter.x + cos(angleInRadians+desviation) * centerDistance,
                    firstCenter.y - sin(angleInRadians+desviation) * centerDistance
                )
            }
            // Diagonal Hacia Abajo : Cuando es hacia abajo se utiliza el primer punto como referencia
            VerticalDirection.DOWN
            -> {
                firstCenter
            }
        }
        while ((if (direction == VerticalDirection.UP) newPoint.y <= down else newPoint.y >= minimunRadius) &&
            if (direction == VerticalDirection.DOWN) newPoint.x > minimunRadius else
                newPoint.x <= imageToCheck.width - minimunRadius
        ) {
            val hole = getHoleFromImage(newPoint, imageToCheck)
            centers.add(hole.center)
            holesbyImage.add(hole)
            holesbyImage.addAll(
                horizontalGeometryReplication(
                    imageToCheck,
                    hole,
                    centerDistance,
                    imageToCheck.width,
                    Direction.LEFT
                )
            )
            assert(true) { "Hasta el momento se han encontrado ${holesbyImage.size}" }
            // System.out.println("New Point " + NewPoint);
            holesbyImage.addAll(
                horizontalGeometryReplication(
                    imageToCheck,
                    hole,
                    centerDistance,
                    imageToCheck.width,
                    Direction.RIGHT
                )
            )
            assert(true) { "Hasta el momento se han encontrado ${holesbyImage.size}" }
            when (direction) {
                VerticalDirection.UP // Diagonal Hacia Arriba
                -> {
                    newPoint = Point()
                    newPoint.x = hole.innerCounture.barycenter.x + cos(angleInRadians+desviation) * centerDistance
                    newPoint.y = hole.innerCounture.barycenter.y - sin(angleInRadians+desviation) * centerDistance
                }
                VerticalDirection.DOWN // Diagonal Hacia Abajo
                -> {
                    newPoint = Point()
                    newPoint.x = hole.innerCounture.barycenter.x - cos(angleInRadians+desviation) * centerDistance
                    newPoint.y = hole.innerCounture.barycenter.y + sin(angleInRadians+desviation) * centerDistance
                }
            }
        }
        return holesbyImage
    }

    private fun horizontalGeometryReplication(
        imageToCheck: Bitmap,
        lastCenterFound: Hole,
        centersDistance: Double,
        widthImage: Int,
        direction: Direction
    ): ArrayList<Hole> {
        val xHolesbyImage: ArrayList<Hole> = ArrayList()
        var newPoint = Point()
        // 20 px es la distancia minimima del radio hasta el borde inferior de la imagen
        val right = imageToCheck.width - minimunRadius
        newPoint = calculateOrientationHorizontal(lastCenterFound.center, centersDistance, direction)
        while (newPoint.x > minimunRadius && newPoint.x < right) {

            val hole = getHoleFromImage(newPoint, imageToCheck)
            if (hole.center.x <= widthImage - minimunRadius) {
                xHolesbyImage.add(hole)
                newPoint = if (hole.innerCounture.initPoint == Point(0.0, 0.0)) {
                    calculateOrientationHorizontal(hole.center, centersDistance, direction)}
                else{
                    desviation=calculateAngle(hole.innerCounture.barycenter,newPoint)
                    calculateOrientationHorizontal(hole.innerCounture.barycenter, centersDistance, direction)}
            }
        }
        return xHolesbyImage
    }

    private fun calculateAngle(p1:Point,p2:Point):Double {
        val pendiente = (p2.y-p2.y)/(p2.y-p2.x)
        return atan(pendiente)
    }

    private fun getHoleFromImage(newPoint: Point, rgbImage: Bitmap): Hole {
        val hole = Hole(center = newPoint)
        val innerCounture = FoilCounture().apply {
            initPoint = getInitContourPoint(
                newPoint, rgbImage, 1, 20,
                Direction.RIGHT, Companion.CountourType.INNER
            )
        }
        if (innerCounture.initPoint == Point(0.0, 0.0)) {
            innerCounture.barycenter = hole.center
        } else {
            innerCounture.apply {
                points = extractContour(innerCounture.initPoint, hole.center, rgbImage, Companion.CountourType.INNER)
                calculateMathStats()
            }
        }
        hole.innerCounture = innerCounture
        val outerCounture = FoilCounture().apply {
            initPoint = getInitContourPoint(
                innerCounture.barycenter, rgbImage,
                (Math.abs(hole.center.x - hole.innerCounture.initPoint.x) + 1).toInt(),
                (Math.abs(hole.center.x - hole.innerCounture.initPoint.x) + 20).toInt(),
                Direction.RIGHT, Companion.CountourType.OUTER
            )
        }
        outerCounture.apply {
            points = extractContour(this.initPoint, hole.center, rgbImage, Companion.CountourType.OUTER)
            calculateMathStats()
        }
        hole.outerCounture = outerCounture
        return hole
    }

    private fun extractContour(
        centroide: Point,
        prelimCenter: Point,
        imageToCheck: Bitmap,
        type: CountourType
    ): ArrayList<Point> {
        val evaluationCompleted = ArrayList<Point>()
        val waitingEvaluation = ArrayList<Point>()
        val blackPoints = ArrayList<Point>()
        val countourPoints = ArrayList<Point>()
        // ArrayList<Point> TotalQuadrant =  new ArrayList<Point> ();
        //Imgproc.cvtColor(imageToCheck, imageToCheck, Imgproc.COLOR_RGB2GRAY)
        var isPoint = false
        var iniPointContour = Point()
        iniPointContour.x = centroide.x
        iniPointContour.y = centroide.y
        if (centroide.x != 0.0 && centroide.y != 0.0) {
            for (i in (iniPointContour.x.toInt() - 1) until (iniPointContour.x.toInt() + 1)) {
                for (j in (iniPointContour.y.toInt() - 1) until (iniPointContour.y.toInt() + 1)) {
                    if (i >= 0 && i < imageToCheck.width) {
                        val pixelValue = imageToCheck.getPixel(i, j)
                        val pColorNeighbor = pixelValue
                        if (pColorNeighbor == Color.BLACK) {
                            isPoint = true
                            val son = Point()
                            son.x = i.toDouble()
                            son.y = j.toDouble()
                            iniPointContour = son
                            break
                        }
                    }
                }
            }
            waitingEvaluation.add(iniPointContour)
            waitingEvaluation.add(prelimCenter)
            // Si encontró un punto inicial para recorrer el contorno y extraer los puntos
            if (isPoint) {
                do {
                    if (!blackPoints.contains(waitingEvaluation[0])) {
                        blackPoints.add(waitingEvaluation[0])
                    }
                    val initPointOfThisContour = waitingEvaluation[0]
                    if (!evaluationCompleted.contains(initPointOfThisContour)) {
                        var blackCount=0
                        for (i in (initPointOfThisContour.x.toInt() - 1)..(initPointOfThisContour.x.toInt() + 1)) {
                            for (j in (initPointOfThisContour.y.toInt() - 1)..(initPointOfThisContour.y.toInt() + 1)) {
                                if (i >= 0 && i < imageToCheck.width && j > 0 && j < imageToCheck.height) {
                                    val pixelValue = imageToCheck.getPixel(i, j)
                                    val pColorNeighbor = pixelValue
                                    val valueToSearch = if (type == Companion.CountourType.INNER) Color.WHITE
                                    else Color.BLACK
                                    if (pColorNeighbor == valueToSearch) {
                                        val pointToEvaluate = Point()
                                        pointToEvaluate.x = i.toDouble()
                                        pointToEvaluate.y = j.toDouble()
                                        blackCount++
                                        if (!waitingEvaluation.contains(pointToEvaluate)) {
                                            waitingEvaluation.add(pointToEvaluate)
                                        }
                                        imageToCheck.setPixel(i, j, 0)
                                    }
                                }
                            }
                        }
                    }
                    waitingEvaluation.remove(initPointOfThisContour)
                    evaluationCompleted.add(iniPointContour)
                } while (waitingEvaluation.size > 0)
            }
        }
        var left= mutableListOf<Point>()
        var right= mutableListOf<Point>()
        /*
        if(blackPoints.size>0){
            extractContour(iniPointContour,iniPointContour,imageToCheck,type)
        }
        */
        for(point in blackPoints){
            var circleCount=0
            for (i in (point.x.toInt() - 1)..(point.x.toInt() + 1)){
                for (j in (point.y.toInt() - 1)..(point.y.toInt() + 1)){
                    if(blackPoints.contains(Point(i.toDouble(),j.toDouble()))){
                        circleCount++
                    }
                }
            }
            if(circleCount<=6){
                countourPoints.add(point)
            }
            if(point.x <=prelimCenter.x){
                left.add(point)
                //left=blackPoints.sortedWith(compareBy { it.y }).associateBy { it.y }.values.toMutableList()
            }
            else{
                right.add(point)
                //right=blackPoints.sortedWith(compareByDescending{ it.x }).associateBy { it.x }.values.toMutableList()
            }
        }

        blackPoints.clear()
        blackPoints.addAll(left)
        blackPoints.addAll(right)
        blackPoints.clear()
        blackPoints.addAll(countourPoints)
        /*
        val innerMat=Mat(imageToCheck.width,imageToCheck.height, CvType.CV_32SC1)
        for (black in blackPoints){
            val data:IntArray= IntArray(1){Color.WHITE}
            innerMat.put(black.y.toInt(), black.x.toInt(),data)
        }
        val converted=Mat()
        innerMat.convertTo(converted,CvType.CV_8UC1)
        val countours= mutableListOf<MatOfPoint>()
        val hierarchy=Mat()
        Imgproc.findContours(converted,countours,hierarchy,Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE)
        blackPoints.clear()
        for (countour in countours){
            blackPoints.addAll(countour.toList())
        }
        */

        //Imgproc.cvtColor(imageToCheck, imageToCheck, Imgproc.COLOR_GRAY2RGB)
        return blackPoints
    }

    private fun getInitContourPoint(
        centroide: Point, biImage: Bitmap, xFrom: Int, xTo: Int,
        direction: Direction, type: CountourType
    ): Point {
        var initPoint = Point(0.0, 0.0)
        var pointToEvaluate = Point(0.0, 0.0)

        loopPoint@ for (xHor in xFrom..xTo) {
            // Si el punto inicial del contorno a la derecha es mayor o igual al ancho de la imagen se sale
            if (centroide.x + xHor >= biImage.width || centroide.x - xHor <= 0) {
                break@loopPoint
            }
            if (centroide.y > biImage.height || centroide.y < 0) {
                break@loopPoint
            }
            if (direction == Direction.RIGHT) {
                pointToEvaluate = Point(centroide.x + xHor, (centroide.y))
            }
            if (direction == Direction.LEFT) {
                pointToEvaluate = Point(centroide.x - xHor, (centroide.y))
            }
            val pixelValue = biImage.getPixel(pointToEvaluate.x.toInt(), pointToEvaluate.y.toInt())
            val valueToCheck = Color.BLACK
            if (pixelValue == valueToCheck) {
                initPoint = pointToEvaluate
                break
            }
        }
        if (initPoint.x == 0.0 && initPoint.y == 0.0 && direction == Direction.RIGHT) {
            initPoint = getInitContourPoint(centroide, biImage, xFrom, xTo, Direction.LEFT, type)
        }
        return initPoint
    }

    private fun calculateOrientationHorizontal(
        lastCenterFound: Point,
        centersDistance: Double,
        direction: Direction
    ): Point =
        when (direction) {
            Direction.RIGHT // Hacia la Derecha
            -> {
                Point(
                    lastCenterFound.x + (centersDistance* cos(desviation)),
                    lastCenterFound.y
                )
            }
            Direction.LEFT //  Hacia la izquierda
            -> {
                Point(
                    (lastCenterFound.x - (centersDistance* cos(desviation))),
                    lastCenterFound.y
                )
            }
        }

    private var compQ1: Comparator<Point> = Comparator {
        // Comparacion para el 1er cuadrante, cuando es X e Y son mayores que el centro preliminar
            P1, P2 ->
        if (P2.x.compareTo(P1.x.toInt()) == 0) {
            P2.y.compareTo(P1.y.toInt())
        } else {
            P2.x.compareTo(P1.x.toInt())
        }
    }

    private var compQ2: Comparator<Point> = Comparator { P1, P2 ->
        if (P2.x.compareTo(P1.x.toInt()) == 0) {
            P1.y.compareTo(P2.y.toInt())
        } else P2.x.compareTo(P1.x.toInt())
    }

    private var compQ3: Comparator<Point> = Comparator { P1, P2 ->
        if (P1.x.compareTo(P2.x.toInt()) == 0) {
            P1.y.compareTo(P2.y.toInt())
        } else P1.x.compareTo(P2.x.toInt())
    }

    private var compQ4: Comparator<Point> = Comparator { P1, P2 ->
        if (P1.x.compareTo(P2.x.toInt()) == 0) P2.y.compareTo(P1.y.toInt())
        else P1.x.compareTo(P2.x.toInt())
    }

    companion object {

        enum class ReplicationDirection {
            DIAGONAL,
            HORIZONTAL
        }

        enum class Direction {
            RIGHT,
            LEFT
        }

        enum class VerticalDirection {
            UP,
            DOWN
        }

        enum class CountourType {
            INNER,
            OUTER
        }

        const val minimunRadius = 15
        const val centerDistance = 80.0
        var angleInRadians = 60 * (Math.PI / 180.0f)

    }
}
