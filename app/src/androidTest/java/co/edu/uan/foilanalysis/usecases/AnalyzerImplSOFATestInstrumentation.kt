package co.edu.uan.foilanalysis.usecases

import org.junit.Test

import org.junit.Assert.*
import org.opencv.core.Point
import java.util.*

class AnalyzerImplSOFATestInstrumentation {

    private var compQ1: Comparator<Point> = Comparator {
        // Comparacion para el 1er cuadrante, cuando es X e Y son mayores que el centro preliminar
            P1, P2 ->
        if (P2.x.compareTo(P1.x.toInt()) == 0) {
            P2.y.compareTo(P1.y.toInt())
        } else {
            P2.x.compareTo(P1.x.toInt())
        }
    }

    @Test
    fun q1FarQ1() {
        val pointA = Point(5.0, 6.0)
        val pointB = Point(6.0, 7.5)
        val points = mutableListOf(pointA, pointB)
        Collections.sort(points, compQ1)
        assertEquals(pointA, points[0])
    }

    @Test
    fun setCompQ1() {
    }

    @Test
    fun getCompQ2() {
    }

    @Test
    fun setCompQ2() {
    }

    @Test
    fun getCompQ3() {
    }

    @Test
    fun setCompQ3() {
    }

    @Test
    fun getCompQ4() {
    }

    @Test
    fun setCompQ4() {
    }
}