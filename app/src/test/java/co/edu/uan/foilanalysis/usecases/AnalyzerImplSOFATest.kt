package co.edu.uan.foilanalysis.usecases

import org.junit.Assert.*
import org.junit.Test
import org.opencv.core.Point
import java.util.*

class AnalyzerImplSOFATest {

    private var compQ1: Comparator<Point> = Comparator {
        // Comparacion para el 1er cuadrante, cuando es X e Y son mayores que el centro preliminar
            P1, P2 ->
        if (P2.x.compareTo(P1.x.toInt()) == 0) {
            P2.y.compareTo(P1.y.toInt())
        } else {
            P2.x.compareTo(P1.x.toInt())
        }
    }

    private var compQ2: Comparator<Point> = Comparator { P1, P2 ->
        if (P2.x.compareTo(P1.x.toInt()) == 0) {
            P1.y.compareTo(P2.y.toInt())
        } else P2.x.compareTo(P1.x.toInt())
    }

    @Test
    fun q1FarQ1() {
        val pointA = Point(5.0, 6.0)
        val pointB = Point(6.0, 7.5)
        val pointC = Point(7.0, 8.0)
        val points = mutableListOf(pointA, pointB, pointC)
        Collections.sort(points, compQ1)
        assertEquals(pointC, points[0])
        val flist: List<Point> = points.sortedWith(compareBy({ it.x }, { it.y })).reversed()
        assertEquals(points.sortedWith(compareBy({ it.x }, { it.y })).reversed().first(), pointC)
        assertEquals(points.sortedWith(compareBy({ it.x }, { it.y })).reversed(), points)
    }

    @Test
    fun q1FarxQ1() {
        val pointA = Point(5.0, 6.0)
        val pointB = Point(6.0, 5.5)
        val pointC = Point(7.0, 5.5)
        val points = mutableListOf(pointA, pointB, pointC)
        Collections.sort(points, compQ1)
        assertEquals(pointC, points[0])
        assertEquals(points.sortedWith(compareBy({ it.x }, { it.y })).reversed().first(), pointC)
        assertEquals(points.sortedWith(compareBy({ it.x }, { it.y })).reversed(), points)
    }

    @Test
    fun q1FaryQ1() {
        val pointA = Point(7.0, 6.0)
        val pointB = Point(6.0, 7.5)
        val pointC = Point(7.0, 5.5)
        val points = mutableListOf(pointA, pointB, pointC)
        Collections.sort(points, compQ1)
        assertEquals(pointA, points[0])
        assertEquals(points.sortedWith(compareBy({ it.x }, { it.y })).reversed().first(), pointA)
        assertEquals(points.sortedWith(compareBy({ it.x }, { it.y })).reversed(), points)
    }

    @Test
    fun q1NearQ1() {
        val pointA = Point(7.0, 8.0)
        val pointB = Point(6.0, 7.5)
        val pointC = Point(5.0, 5.5)
        val points = mutableListOf(pointA, pointB, pointC)
        Collections.sort(points, compQ1)
        assertEquals(pointA, points[0])
        assertEquals(points.sortedWith(compareBy({ it.x }, { it.y })).reversed().first(), pointA)
        assertEquals(points.sortedWith(compareBy({ it.x }, { it.y })).reversed(), points)
    }

    @Test
    fun q1EqualxQ1() {
        val pointA = Point(6.0, 8.0)
        val pointB = Point(6.0, 7.5)
        val pointC = Point(6.0, 5.5)
        val points = mutableListOf(pointA, pointB, pointC)
        Collections.sort(points, compQ1)
        assertEquals(pointA, points[0])
        assertEquals(points.sortedWith(compareBy({ it.x }, { it.y })).reversed().first(), pointA)
        assertEquals(points.sortedWith(compareBy({ it.x }, { it.y })).reversed(), points)
    }

    @Test
    fun q1EqualyQ1() {
        val pointA = Point(7.0, 8.0)
        val pointB = Point(6.0, 8.0)
        val pointC = Point(5.0, 8.0)
        val points = mutableListOf(pointA, pointB, pointC)
        Collections.sort(points, compQ1)
        assertEquals(pointA, points[0])
        assertEquals(points.sortedWith(compareBy({ it.x }, { it.y })).reversed().first(), pointA)
        assertEquals(points.sortedWith(compareBy({ it.x }, { it.y })).reversed(), points)
    }

    @Test
    fun q1Testing() {
        q1FarQ1()
        q1FarxQ1()
        q1FaryQ1()
        q1EqualxQ1()
        q1EqualyQ1()
        q1NearQ1()
    }

    @Test
    fun q2FarQ1() {
        val pointA = Point(5.0, 6.0)
        val pointB = Point(6.0, 7.5)
        val points = mutableListOf(pointA, pointB)
        Collections.sort(points, compQ2)
        assertEquals(pointB, points[0])
        // assertEquals(points.sortedWith(compareByDescending<Point>{it.x}.thenBy { it.y }).first(),pointB)
    }

    @Test
    fun q2FarxQ1() {
        val pointA = Point(5.0, 6.0)
        val pointB = Point(6.0, 5.5)
        val points = mutableListOf(pointA, pointB)
        Collections.sort(points, compQ2)
        assertEquals(pointB, points[0])
        // assertEquals(points.sortedWith(compareByDescending<Point>{it.x}.thenBy { it.y }).first(),pointB)
    }

    @Test
    fun q2FaryQ1() {
        val pointA = Point(7.0, 6.0)
        val pointB = Point(6.0, 7.5)
        val points = mutableListOf(pointA, pointB)
        Collections.sort(points, compQ2)
        assertEquals(pointA, points[0])
        // assertEquals(points.sortedWith(compareByDescending<Point>{it.x}.thenBy { it.y }).first(),pointA)
    }

    @Test
    fun q2NearQ1() {
        val pointA = Point(7.0, 8.0)
        val pointB = Point(6.0, 7.5)
        val points = mutableListOf(pointA, pointB)
        Collections.sort(points, compQ2)
        assertEquals(pointA, points[0])
        // assertEquals(points.sortedWith(compareByDescending<Point>{it.x}.thenBy { it.y }).first(),pointA)
    }
}